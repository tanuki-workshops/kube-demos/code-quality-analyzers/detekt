# Code Quality Detekt for GitLab CI

## Example

```yaml
stages:
  - 🔎code-quality
  
include:
  - project: 'tanuki-workshops/kube-demos/code-quality-analyzers/detekt'
    file: 'detekt.gitlab-ci.yml'

🔎:code:quality:detekt:
  stage: 🔎code-quality
  extends: .detekt:analyzer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  script:
    - analyze src
```
